import "../../../styles/index.scss";

const activeOrdersEl = document.querySelector("#activeOrders");
const prevOrdersEl = document.querySelector("#prevOrders");
let orders = JSON.parse(localStorage.getItem("orders"));

const restaurantNames = ["Domino’s Pizza", "McDonald’s", "KFC"];
const MAX_ORDER_TIME = 60;

function validateOrder(order) {
  if (order.id <= 0) {
    throw new Error(`Order-id is less than or equal to 0`);
  }

  if (order.price <= 0) {
    throw new Error(`Order price is less than or equal to 0`);
  }

  if (order.title.length < 5 || order.title.length > 30) {
    throw new Error(`${order.title}, title is not valid`);
  }

  if (order.count <= 0) {
    throw new Error(`Count is less than or equal to 0`);
  }
}

class Checkout {
  #orders = [];
  #checkoutTime = null;
  #restaurant = "";

  constructor(data) {
    this.#restaurant = data.restaurant;
    this.#orders = data.orders;
    this.#checkoutTime = data.checkout;

    this.getRestaurant();
    this.getOrders();
    this.getCheckoutTime();
  }

  getRestaurant() {
    if (typeof this.#restaurant !== "string") {
      throw new Error(`Restaurant name is not a string`);
    }

    if (restaurantNames.includes(this.#restaurant)) {
      return this.#restaurant;
    } else {
      throw new Error(`This restaurant doesn't exist.`);
    }
  }

  getOrders() {
    this.#orders.forEach(validateOrder);

    return this.#orders;
  }

  getCheckoutTime() {
    const diff = new Date() - new Date(this.#checkoutTime);

    if (diff <= 0) {
      throw new Error(`Order date cannot be earlier than today's date`);
    }

    return new Date(this.#checkoutTime);
  }

  getFormattedDate() {
    const day = this.getCheckoutTime().getDate();
    const mounthName = this.getCheckoutTime().toLocaleDateString("default", {
      month: "long",
    });
    const year = this.getCheckoutTime().getFullYear();

    return `${day} ${mounthName} ${year}`;
  }

  getFormattedTime() {
    const result = this.getCheckoutTime().toLocaleTimeString("en-US", {
      hour: "2-digit",
      minute: "2-digit",
    });
    return result;
  }

  getElapsedTimeInMinutes() {
    const diffInMilliseconds = Date.now() - this.getCheckoutTime();
    const diffInMinutes = Math.floor(
      diffInMilliseconds / 1000 / MAX_ORDER_TIME
    );
    return diffInMinutes;
  }

  getCheckoutTimePercent = () => {
    return Math.floor(
      (Math.min(this.getElapsedTimeInMinutes(), MAX_ORDER_TIME) /
        MAX_ORDER_TIME) *
        100
    );
  };

  get isOrderFinished() {
    return this.getElapsedTimeInMinutes() > MAX_ORDER_TIME;
  }
}

orders = orders.map((it) => new Checkout(it));

const activeOrders = orders.filter((it) => !it.isOrderFinished);

activeOrdersEl.innerHTML = activeOrders.map(getActiveOrderCardHtml).join("");

function getActiveOrderCardHtml(order) {
  return `
            <div class="coming-up__item coming-up-item" >
            <div class="coming-up-item__header">
              <h4 class="h4">${order.getRestaurant()}</h4>
              <div class="badge badge--orange">Доставка</div>
            </div>

            <div class="coming-up-info">
              <img src="img/icons/clock.svg" alt="">
              <div class="coming-up-info__content">
                <div>Заказ будет доставлен через</div>
                <div class="coming-up-info__title">${
                  MAX_ORDER_TIME - order.getElapsedTimeInMinutes()
                } мин</div>
              </div>
            </div>

            <div class="progress-bar">
              <div class="progress-bar__line" style="width: ${order.getCheckoutTimePercent()}%"></div>
              <div class="progress-bar__overlay">
                <div class="progress-bar__item progress-bar__item--first"></div>
                <div class="progress-bar__item progress-bar__item--sec"></div>
                <div class="progress-bar__item progress-bar__item--third"></div>
              </div>
            </div>
          </div>
          `;
}

const prevOrders = orders.filter((it) => it.isOrderFinished);
prevOrdersEl.innerHTML = prevOrders.map(getPrevOrderCardHtml).join("");

function getPrevOrderCardHtml(order) {
  return `
  <div class="previous__item previous-item">
  <div class="previous-item__header">
    <h4 class="h4">${order.getRestaurant()}</h4>
    <div class="badge badge--green">Выполнен</div>
  </div>

  <div class="previous-item-info">
    <div class="previous-item-info__date">${order.getFormattedDate()}</div>
    <div class="previous-item-info__time">${order.getFormattedTime()}</div>
  </div>

  <ul class="previous-item-dishes">
    ${order
      .getOrders()
      .map((obj) => {
        return `<li class="previous-item-dishes__item">
      <span class="previous-item-dishes__quantity">${obj.count}</span>
      ${obj.title}
    </li>`;
      })
      .join("")}
  </ul>
</div>
`;
}
