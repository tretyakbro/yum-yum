import "../../../styles/index.scss";
import dominosData from "./data/dominos.json";
import kfcData from "./data/kfc.json";
import macData from "./data/mac.json";

const restaurantListEl = document.getElementById("restaurantList");
const dishesEl = document.getElementById("dishes");
const cardEl = document.querySelector("#basketIcon");
const drawerMainEl = document.querySelector(".drawer__main");
const checkoutBtnEl = document.querySelector("#checkout");

let cardArr = [];
let ordersArr = [];

let selectedCategoryId = 0;
let restaurantName = "Domino’s Pizza";
let deliveryPrice = "бесплатная доставка";
let totalPrice = 0;

class Dish {
  #count;

  constructor(data) {
    this.id = data.id;
    this.price = data.price;
    this.title = data.title;
    this.img = data.img;
    this.#count = data.count;
  }

  getCount() {
    return this.#count;
  }

  setCount(num) {
    if (typeof num !== "number") {
      throw new Error(`Counter value is not number`);
    }

    if (num < 0) {
      throw new Error(`Counter value must not be less than 0`);
    }

    this.#count = num;
  }
}

const restaurantProducts = {
  0: dominosData.map((it) => new Dish(it)),
  1: macData.map((it) => new Dish(it)),
  2: kfcData.map((it) => new Dish(it)),
};

const resets = () => {
  cardArr = [];
  cardEl.querySelector("#basket-badge").innerHTML = cardArr.length;
  restaurantProducts[selectedCategoryId].forEach((obj) => obj.setCount(0));
};

restaurantListEl.addEventListener("click", (e) => {
  const restaurantCardEl = e.target.closest(".featured-item");
  if (restaurantCardEl) {
    selectedCategoryId = restaurantCardEl.dataset.id;

    restaurantName =
      selectedCategoryId === "0"
        ? "Domino’s Pizza"
        : selectedCategoryId === "1"
        ? "McDonald’s"
        : "KFC";

    deliveryPrice =
      restaurantCardEl.querySelector(".badge").dataset.deliveryPrice === "0"
        ? "бесплатная доставка"
        : "20 грн";

    restaurantListEl.querySelector(".active").classList.remove("active");
    restaurantCardEl.classList.add("active");

    resets();

    dishesEl.innerHTML = restaurantProducts[selectedCategoryId]
      .map((obj) => getProductCartHtml(obj))
      .join("");
  }
});

const addProductToCard = (obj) => {
  const isExist = cardArr.find((el) => el.id === obj.id);

  if (!isExist) {
    cardArr.push(obj);
    cardEl.querySelector("#basket-badge").innerHTML = cardArr.length;
  }

  if (obj.getCount() === 0) {
    cardArr = cardArr.filter((el) => el.id !== obj.id);
    cardEl.querySelector("#basket-badge").innerHTML = cardArr.length;
  }
};

function getProductCardCounterHTML(num) {
  return `<div class="counter" id="counter">
              <button style="display: ${
                num === 0 ? "none" : "block"
              };" id="decreaseButton" class="counter__button counter__button--decrease"></button>
              <span id="counterNumber" class="counter__number">${num}</span>
              <button id="increaseButton" class="counter__button counter__button--increase"></button>
            </div>`;
}

function getProductCartHtml(obj) {
  const { id, price, title, img } = obj;

  return `
          <div id="dish-${id}" data-id="${id}" class="dish">
          <img class="dish__image" src="${img}" alt="">
          <div class="dish__title">${title}</div>
          <div class="dish__info">
            <div class="dish__price">${price} грн</div>
            ${getProductCardCounterHTML(obj.getCount())}
          </div>
        </div>`;
}

dishesEl.innerHTML = restaurantProducts[0]
  .map((obj) => getProductCartHtml(obj))
  .join("");

dishesEl.addEventListener("click", (e) => {
  if (e.target.closest(".counter__button--increase")) {
    const dataId = e.target.closest(".dish").dataset.id;

    const obj = restaurantProducts[selectedCategoryId].find(
      (el) => el.id === Number(dataId)
    );

    obj.setCount(obj.getCount() + 1);

    getProductCardCounterHTML(obj.getCount());

    const currentProductCardEl = document.getElementById(`dish-${dataId}`);
    const productCardCounterEl = currentProductCardEl.querySelector("#counter");

    productCardCounterEl.outerHTML = getProductCardCounterHTML(obj.getCount());

    addProductToCard(obj);
  }

  if (e.target.closest(".counter__button--decrease")) {
    const dataId = e.target.closest(".dish").dataset.id;

    const obj = restaurantProducts[selectedCategoryId].find(
      (el) => el.id === Number(dataId)
    );

    obj.setCount(obj.getCount() - 1);

    getProductCardCounterHTML(obj.getCount());

    const currentProductCardEl = document.getElementById(`dish-${dataId}`);
    const productCardCounterEl = currentProductCardEl.querySelector("#counter");

    productCardCounterEl.outerHTML = getProductCardCounterHTML(obj.getCount());

    addProductToCard(obj);
  }
});

const getOrdersHtml = (obj) => {
  return `
    <div class="order__item order-item">
    <img class="order-item__image" src="${obj.img}" alt="">
    <span class="order-item__quantity">${obj.getCount()} X</span>
    <div class="order-item__info">
      <h3 class="order-item__title h3">${obj.title}</h3>
      <div class="order-item__price">${obj.price} грн</div>
    </div>
    <button class="icon-button icon-button--red"><img src="img/icons/delete.svg" alt=""></button>
  </div>
  `;
};

const getTotalPrice = () => {
  totalPrice = 0;
  cardArr.forEach((obj) => (totalPrice += obj.price * obj.getCount()));
  if (deliveryPrice === "20 грн") totalPrice += 20;
  return totalPrice;
};

cardEl.addEventListener("click", (e) => {
  document.querySelector(".overlay").classList.add("visible");

  drawerMainEl.querySelector("#restaurantTitle").textContent = restaurantName;

  drawerMainEl.querySelector(".order").innerHTML = cardArr.map(getOrdersHtml);

  drawerMainEl.querySelector("#deliveryPrice").innerHTML = deliveryPrice;
  drawerMainEl.querySelector(
    "#totalPrice"
  ).innerHTML = `(${getTotalPrice()}  грн)`;
});

drawerMainEl.addEventListener("click", (e) => {
  if (e.target.closest("#closeCart") || e.target.closest(".add-more")) {
    document.querySelector(".overlay").classList.remove("visible");
  }

  // if (e.target.closest("#checkout")) {
  //   // const orderObj = {
  //   //   checkout: new Date(),
  //   //   orders: cardArr,
  //   //   restaurant: restaurantName,
  //   // };

  //   ordersArr.push(orderObj);

  //   localStorage.setItem("orders", JSON.stringify(ordersArr));

  //   window.location.href = "orders.html";
  // }
});

checkoutBtnEl.addEventListener("click", (e) => {
  if (cardArr.length) {
    const checkoutData = {
      orders: cardArr.map((it) => {
        const { id, title, price } = it;
        return { id, title, price, count: it.getCount() };
      }),
      checkout: new Date(),
      restaurant: restaurantName,
    };

    if (localStorage.getItem("orders")) {
      let data = JSON.parse(localStorage.getItem("orders"));
      data.push(checkoutData);
      localStorage.setItem("orders", JSON.stringify(data));
      window.location.href = "orders.html";
    } else {
      ordersArr.push(checkoutData);
      localStorage.setItem("orders", JSON.stringify(ordersArr));
      window.location.href = "orders.html";
    }
  }
});
